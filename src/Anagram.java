public class Anagram {

	////////////////////////////////////////////////////////////////////////////
	private static void buubleSort(char tab[]) {
		char temp;
		int zmiana = 1;
		while (zmiana > 0) {
			zmiana = 0;
			for (int i = 0; i < tab.length - 1; i++) {
				if (tab[i] > tab[i + 1]) {
					temp = tab[i + 1];
					tab[i + 1] = tab[i];
					tab[i] = temp;
					zmiana++;
				}
			}
		}
		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i]);
		}
	}
	///////////////////////////////////////////////////////////////////////////
	private static  boolean isAnagram0(String s1, String s2){
		
		char[] alphabet = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's',
				't', 'u', 'w', 'x', 'y', 'z' };
		// int[] letterCounter = new int[alphabet.length]; //letterCounter[0]=a
		// letterCounter[1]=� ...
		int[] letterCounterForS1 = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		int[] letterCounterForS2 = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		for (int j = 0; j < alphabet.length; j++) {
			// i leci po stringu, j leci po tablicach alphabet oraz
			// letterCounter
			for (int i = 0; i < s1.length(); i++) {
				if (s1.charAt(i) == alphabet[j]) {
					letterCounterForS1[j]++;
				}
			}
			for (int i = 0; i < s2.length(); i++) {
				if (s2.charAt(i) == alphabet[j]) {
					letterCounterForS2[j]++;
				}
			}
		}

		if (letterCounterForS1.equals(letterCounterForS2)) {
			return true;
		}

		return false;
	}

	///////////////////////////////////////////////////////////////////////////
	private static boolean isAnagram1(String s1, String s2) {

		int counter = 0;

		if (s1.length() == s2.length()) {

			for (int i = 0; i < s1.length(); i++) {

				for (int j = 0; j < s2.length(); j++) {
					if (s1.charAt(i) == s2.charAt(j)) {
						counter++;
					}
				}
			}

		} else {
			System.out.println(s1 + " oraz " + s2 + " nie s� rownej dlugosci");
		}

		if (counter == s1.length()) { // moze byc tez s2.length
			return true;
		}

		return false;
	}

	/////////////////////////////////////////////////////////////////////////////
	private static boolean isAnagram2(String s1, String s2) {

		char[] t1 = new char[s1.length()];
		char[] t2 = new char[s2.length()];
		int counter = 0;

		for (int i = 0; i < t1.length; i++) {

			t1[i] = s1.charAt(i);
			t2[i] = s2.charAt(i); // tutaj mamy znaki ze stringow w tablicach
		}

		System.out.println("t1[]:");
		for (int i = 0; i < t1.length; i++) {
			System.out.println(t1[i]);
		}
		System.out.println("t2[]:");
		for (int i = 0; i < t2.length; i++) {
			System.out.println(t2[i]);
		}
		System.out.println("*****************");
		System.out.println("***sortowanie***");
		System.out.println("*****************");

		// posortowac obie tablice char�w
		System.out.println("t1[]-->posortowana:");
		buubleSort(t1);// wywolanie tej funkcji z parametrem t1 zmienia tablice
						// na posortowana w tym scopie
		System.out.println("t2[]-->posortowana:");
		buubleSort(t2);

		for (int i = 0; i < t1.length; i++) {
			if (t1[i] == t2[i]) {
				counter++;
			}
		}

		if (counter == t1.length) {
			return true;
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	public static void main(String[] args) {
		
		System.out.println(isAnagram0("post", "stop"));

//		System.out.println(
//				"is anagram: " + isAnagram2("kandydat", "dyktanda") + "\n///////////////" + "\n///////////////");
//		System.out.println("is anagram: " + isAnagram2("stoper", "proste") + "\n///////////////" + "\n///////////////");
//		System.out.println("is anagram: " + isAnagram2("post", "stop") + "\n///////////////" + "\n///////////////");
		
	}

	
	
}
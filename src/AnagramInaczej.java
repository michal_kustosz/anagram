
public class AnagramInaczej {

	private static boolean isAnagram0(String s1, String s2) {

		char[] alphabet = { 'a', 'b', 'c', 'd', 'e', 
							'f', 'g', 'h', 'i', 'j', 
							'k', 'l', 'm', 'n', 'o', 
							'p', 'r', 's','t', 'u', 
							'w', 'x', 'y', 'z' };
		// int[] letterCounter = new int[alphabet.length]; //letterCounter[0]=a
		// letterCounter[1]=a ...
		int[] letterCounterForS1 = { 0, 0, 0, 0, 0, 
									 0, 0, 0, 0, 0, 
									 0, 0, 0, 0, 0, 
									 0, 0, 0, 0, 0, 
									 0, 0, 0, 0 };

		
		int[] letterCounterForS2 = { 0, 0, 0, 0, 0, 
									 0, 0, 0, 0, 0, 
									 0, 0, 0, 0, 0, 
									 0, 0, 0, 0, 0, 
									 0, 0, 0, 0 };
		

		for (int j = 0; j < alphabet.length; j++) {
			// i leci po stringu, j leci po tablicach alphabet oraz
			// letterCounterS1 i letterCounterS1
			for (int i = 0; i < s1.length(); i++) {
				if (s1.charAt(i) == alphabet[j]) {
					letterCounterForS1[j]++;
				}
			}
			for (int i = 0; i < s2.length(); i++) {
				if (s2.charAt(i) == alphabet[j]) {
					letterCounterForS2[j]++;
				}
			}
		}

		int counterToCompareArrays = 0;

		if (letterCounterForS1.length == letterCounterForS2.length) {

			for (int i = 0; i < letterCounterForS1.length; i++) {
				if (letterCounterForS1[i] == letterCounterForS2[i]) {
					counterToCompareArrays++;
				}
			}
		}
		if (counterToCompareArrays == letterCounterForS1.length) {
			return true;
		}

		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	public static void main(String[] args) {
		
		System.out.println("kandydat, dyktanda: "+isAnagram0("kandydat", "dyktanda"));
		System.out.println("post, stop: "+isAnagram0("post", "stop"));
		System.out.println("qwertyuiop, poiuytrewq: "+isAnagram0("qwertyuiop", "poiuytrewq"));
		System.out.println("stoper, proste: "+isAnagram0("stoper", "proste"));
		System.out.println("qqeeercxz, qxzceeerq: "+isAnagram0("qqeeercxz", "qxzceeerq"));
		System.out.println("anna madrigal  , a man and a girl: "+isAnagram0("anna madrigal  ", "a man and a girl"));
		
		System.out.println("**************************************************************");
		
		System.out.println("ala, kot: "+isAnagram0("ala", "kot"));
		System.out.println("stolik, fortepian: "+isAnagram0("stolik", "fortepian"));
		
	}

}
